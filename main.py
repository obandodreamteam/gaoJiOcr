import cv2
import sys
from PyQt5.QtGui import QImage, QPixmap
from PyQt5.QtWidgets import QLabel, QApplication,QFileDialog
import numpy
import tkinter as tk


def numpyImage2qImage(numpyArray):
    tuple = numpyArray.shape
    height = tuple[0]
    width = tuple[1]
    if len(tuple) == 2:
        bytesPerLine = width
        qimage = QImage(numpyArray.data, width, height, bytesPerLine, QImage.Format_Grayscale8)
    elif tuple[2] == 3:
        bytesPerLine = width * 3
        cv2.cvtColor(numpyArray, cv2.COLOR_BGR2RGB, numpyArray)
        qimage = QImage(numpyArray.data, width, height, bytesPerLine, QImage.Format_RGB888)
    return qimage

app = QApplication(sys.argv)
filePath=QFileDialog.getOpenFileName(None, 'Abre imagen','','Imagen (*.png *.jpg *.bpm *.tif)')
if filePath[1]:
    image = cv2.imread(filePath)
    cv2.distanceTransform(bw, dist, CV_DIST_L2, 3);
    image2= numpyImage2qImage(image)
    label = QLabel()
    label.setPixmap(QPixmap.fromImage(image2))
    label.show()
    sys.exit(app.exec_())
